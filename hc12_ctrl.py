"""
Python wrapper for HC-12 module which is connected to RPi via UART.
"""
import serial
from base import Base
import time
import RPi.GPIO as GPIO


class HC12Ctrl(Base):

    ser = None
    set_pin_state = False
    set_pin = 18

    def __init__(self, port='/dev/ttyS0', baudrate=9600, set_pin=18, serial_timeout=2):
        Base.__init__(self)

        self.set_pin = set_pin

        GPIO.setmode(GPIO.BCM)
        # GPIO.cleanup()
        GPIO.setwarnings(False)
        # Set 'SET' pin as output pin
        GPIO.setup(set_pin, GPIO.OUT)
        GPIO.output(set_pin, False)
        self.set_pin_state = False

        self.ser = serial.Serial(port=port, baudrate=baudrate, timeout=serial_timeout)

    def check_hc12(self):
        """
        Check connection with HC-12
        :return:
        """
        self.send_cmd('AT')
        return self.parse_response()

    def parse_response(self):
        if 'OK' in self.read_data():
            return True
        else:
            return False

    def set_serial_baudrate(self, br):
        """
         Set baud rate of serial port, default value is 9600
        :param br: new baud rate
        :return:
        """
        self.send_cmd('AT+B' + str(br))
        return self.parse_response()

    def set_channel(self, ch_num):
        """
        Set radio channel, HC-12 has up to 100 channels
        :param ch_num: channel number
        :return:
        """
        self.send_cmd('AT+C' + str(ch_num))
        return self.parse_response()

    def set_device_mode(self, mode_num):
        """
        Set device mode,
        :param mode_num: Mode number
        :return:
        """
        if mode_num not in [1, 2, 3, 4]:
            print 'Unknown mode number'
            return False

        self.send_cmd('AT+FU' + str(mode_num))
        return self.parse_response()

    def set_trans_power(self, power_num):
        """
        Set device transmitting power
        :param power_num: range 1 ~ 8
        :return:
        """
        if power_num not in [1, 2, 3, 4, 5, 6, 7, 8]:
            print 'Unknown power number'
            return False

        self.send_cmd('AT+P' + str(power_num))
        return self.parse_response()

    def get_all_settings(self):
        """
        Retrieve all parameters: mode, channel, baud rate, power
        :return:
        """
        self.send_cmd('AT+RX')
        return self.read_data()

    def reset_device(self):
        self.send_cmd('AT+DEFAULT')
        return self.parse_response()

    def send_cmd(self, data):
        """
        Check `SET` pin before sending any command
        :param data:
        :return:
        """
        if self.set_pin_state:
            print "Release 'SET' pin to switch to CMD mode."
            GPIO.output(self.set_pin, False)
            self.set_pin_state = False
            time.sleep(.1)

        self.ser.write(data + '\r\n')

    def send_serial_data(self, data):
        """
        Set SET_PIN as high before sending any data
        :param data:
        :return:
        """
        if not self.set_pin_state:
            print "Set 'SET' pin as high to switch to data mode."
            GPIO.output(self.set_pin, True)
            self.set_pin_state = True
            time.sleep(.1)

        return self.ser.write(data)

    def read_data(self):
        """
        Read data from HC-12 until meet carriage return
        :return:
        """
        return self.ser.readall()


if __name__ == '__main__':

    h = HC12Ctrl('/dev/ttyS0')

    print h.get_all_settings()

    time.sleep(2)

    print h.send_serial_data('off')





