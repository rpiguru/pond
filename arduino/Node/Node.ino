/*
 * The PWM frequency of MAX14870 is 50KHz, and the resolution is 256...
 * 
 */
#include <SoftwareSerial.h>
#include <PWM.h>

SoftwareSerial mySerial(2, 3);  //RX, TX
int set_pin = 4;                // SET pin of HC-12
String readString;
int ledPin = 13;

int32_t frequency = 50000; //frequency (in Hz)

void setup() {
  pinMode(set_pin, OUTPUT);
  digitalWrite(set_pin, HIGH);   // Set this pin low when configuring, and set this pin high when transformating...
  pinMode(ledPin, OUTPUT); 
  digitalWrite(ledPin, LOW);

  Serial.begin(9600);
  mySerial.begin(9600);
  
  InitTimersSafe(); 
  bool success = SetPinFrequencySafe(9, frequency);
  if(success) {
    Serial.println("Successfully set frequency.");
  }
}

void loop() {
  
  
  if(Serial.available() > 0){//Read from serial monitor and send over HC-12
    String input = Serial.readString();
    mySerial.println(input);    
  }
 
//  if(mySerial.available() > 1){//Read from HC-12 and send to serial monitor
//    String input = mySerial.readString();
//    Serial.println(input);    
//  }
//  delay(20);

  while (mySerial.available()) {
    delay(3);  
    char c = mySerial.read();
    readString += c; 
  }
  readString.trim();
  if (readString.length() >0) {
    if (readString == "on"){
      Serial.println("switching on");
      digitalWrite(ledPin, HIGH);
      pwmWrite(9, 64);
    }
    if (readString == "off")
    {
      Serial.println("switching off");
      digitalWrite(ledPin, LOW);
      pwmWrite(9, 0);
    }

    readString="";
  } 
  
}
