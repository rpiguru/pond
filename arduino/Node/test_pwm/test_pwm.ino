#include <PWM.h>
int32_t frequency = 50000; //frequency (in Hz)

void setup() {
  InitTimersSafe(); 
  bool success = SetPinFrequencySafe(9, frequency);
  if(success) {
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);    
  }
}

void loop() {
  int sensorValue = analogRead(A0);
  pwmWrite(9, sensorValue / 4);
  delay(30);      
}
