## Wiring components.

1. Working with Arduino pro mini

- Connecting Arduino pro mini with HC-12.
    
    Before connecting, solder the spring antenna and male-jumpers on your HC-12.
    
    | **Arduino** | **HC-12**|  **Color**   |
    | :----:      | :----:   |  :----:      |
    |  GND        |  GND     |   Yellow     |
    |  VCC        |  VCC     |   Orange     |
    |  PIN 2      |  TXD     |   Blue       |
    |  PIN 3      |  RXD     |   Green      |
    |  PIN 4      |  SET     |   Black      |

    Wiring:
    
    ![Wiring Arduino with HC-12](img/arduino-hc12.jpg "Wiring Arduino with HC-12")
    
- Connecting Arduino pro mini with FTDI Converter.
    
    | **Arduino** | **FTDI** |  **Color**   |
    |   :----:    | :----:   |  :----:      |
    |  GND        |  GND     |   Brown      |
    |  VCC        |  PWR     |   Red        |
    |  RXD        |  TXD     |   Green      |
    |  TXD        |  RXD     |   Yellow     |
    |  DTR        |  DTR     |   White      |
    
    Wiring:
    
    ![Wiring Arduino with FTDI](img/arduino-ftdi.jpg "Wiring Arduino with FTDI")


- Connecting Arduino pro mini with DC Motor Driver Board.
        
    | **Arduino** | **Motor Board** |
    |   :----:    | :----:          |
    |  PIN 9        |  PWM     |
    |  PIN 8        |  DIR     |
    
    ![Wiring Arduino with DC Motor Driver](img/img4.jpg "Wiring Arduino with DC Motor Driver")
    
    *Caution:* In the picture above, **RAW** & **GND** pins are connection to the battery.
    
    We do not need these wires in the case of developing, because we will power the Arduino via FTDI converter. 
    
    So, please **do not** wire them until we finish development.
    

- Connect the FTDI Converter to your Window PC through USB/mini USB converter.
    
    And you will need to install its driver.
    
    Download driver from http://www.ftdichip.com/Drivers/VCP.htm and install on your PC.
    
    After that, you could see that it is detected as **COM3** or **COM4** like this:
    
    ![Installing FTDI Driver](img/img3.jpg "Installing FTDI Driver")

    **NOTE:** Once completing development, we do not need FTDI converter any more.
    
    At that time, we will have to power the Arduino Pro mini with Battery & DC Power Converter.
    
    Connect output of battery to the **RAW** pin of Arduino pro mini as I have mentioned in the picture above.
    
    (I was supposed that we need additional DC Power converter, but fortunately Arduino can work with up to 12VDC...)
    

- BreadBoad View
    
    ![BreadBoard](img/schematic_bb.jpg "BreadBoardView")
    
- Schematic View
    
    ![Schematic](img/schematic_schem.jpg "Schematic View")
    
    
2. Working with Raspberry Pi

- Connecting Raspberry Pi with HC-12
    
    | **RPi**     | **HC-12**|  **Color**   |
    | :----:      | :----:   |  :----:      |
    |  GND        |  GND     |   Yellow     |
    |  5V         |  VCC     |   Orange     |
    |  GPIO14     |  RXD     |   Green      |
    |  GPIO15     |  TXD     |   Blue       |
    |  GPIO18     |  SET     |   Black      |
    
    Wiring:
    
    ![Wiring RPi with HC-12](img/rpi-hc12.jpg "Wiring RPi with HC-12")
    
## Give me remote connection to your Windows PC and RPi.

1. Windows PC

    You could use *TeamViewer* for sharing your PC.
    
    https://www.youtube.com/watch?v=KzH7nYzsxSk
    
2. Sharing your Raspberry Pi.

    It'd be easy to use *Dataplicity* service. 
    
    https://www.dataplicity.com
    
    Once signing up, please install dataplicity client on your RPi by following this:
    
    https://www.youtube.com/watch?v=AXE7aAVM0sM 
    
    And share your dataplicity account with me.
    

## Preparing Raspberry Pi

## Preventing Raspberry Pi from using the serial port

The Broadcom UART appears as `/dev/ttyS0` under Linux.

There are several minor things in the way if you want to have dedicated control of the serial port on a Raspberry Pi.

Firstly, the kernel will use the port as controlled by kernel command line contained in `/boot/cmdline.txt`.

The file will look something like this:
    
    dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait
    
The console keyword outputs messages during boot, and the kgdboc keyword enables kernel debugging.

You will need to remove all references to `ttyAMA0`.

So, for the example above `/boot/cmdline.txt`, should contain:

    dwc_otg.lpm_enable=0 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait

You must be root to edit this (e.g. use `sudo nano /boot/cmdline.txt`).

Be careful doing this, as a faulty command line can prevent the system booting.

Secondly, after booting, a login prompt appears on the serial port.

This is controlled by the following lines in `/etc/inittab`:

    #Spawn a getty on Raspberry Pi serial line
    T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100

You will need to edit this file to comment out the second line, i.e.

    #T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100
    
Finally you will need to reboot the Raspberry Pi for the new settings to take effect.

Once this is done, you can use `/dev/ttyS0` like any normal Linux serial port, and you won't get any unwanted traffic confusing the attached devices.

To double-check, use

    cat /proc/cmdline

to show the current kernel command line, and
    
    ps aux | grep ttyS0
    
to search for getty processes using the serial port.